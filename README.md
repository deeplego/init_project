# Template Data Science Projects  
This project is thought as a template and initializer for Data Science projects.

## Structure
```
.  
├── data  
│   ├── interim  
│   ├── processed  
│   └── raw  
├── docs  
├── notebooks  
├── outputs  
│   ├── plots  
│   └── submissions  
├── README.md  
├── src  
│   ├── constants  
│   │   └── paths.py  
│   ├── make_data  
│   └── models  
└── upath.py  
```

## Requirements
* path.py==11.5.0  

## How to use it
```
$ cd <projects_directory>
$ mkdir -p ./<project_name>/src/constants
$ cp ./inizialization/src/constants/paths.py -t ./<project_name>/src/constants
$ python /<project_name>/src/constants/path.py
```

### File description
* ./src/constants/paths.py  
    * contains all paths of the project  
    * needs some tweaks regarding the name of the project  
    * if run directly, it creates all the folders of the structure  
* ./upath.py  
    * update the python path with project directory path  
